package com.teamps.samsungzawgyiandroid4plus;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ScrollView;
import android.widget.TextView;

public class ZWSPActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zwsp);
        // Font path
        String fontPath = "fonts/ZawgyiOne.ttf";
        
        // text view label
        TextView txtZawgyi = (TextView) findViewById(R.id.txtView);
        txtZawgyi.setMovementMethod(new ScrollingMovementMethod());
        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        
        // Applying font
        txtZawgyi.setTypeface(tf);
        
        String myStr = "Samsung ဖုန္း နဲ႔ Android 4.0 Plus ေတြမွာ ေဇာ္ဂ်ီ Font ေတြ လြဲေနတဲ့ ျပႆနာ ရွိေနတယ္။"+
        		"အျဖစ္မ်ားတာကေတာ့ သေဝထိုးလို ငသတ္လိုေနရာမ်ိဳး ေတြပဲ ျဖစ္ပါတယ္။ အဲဒီ ျပႆနာကို ေျဖရွင္းဖို႔ လိုက္ရွာေတာ့ "+
        		"Zero-width space ဆိုတဲ့ concept ကိုသုံးလို႕ ရမယ္လို႕ ထင္မိတယ္။ သေဘာကေတာ့ အဲဒီ ျပႆနာ တတ္ေနတဲ့ "+
        		"characters ေတြကို string ထဲမွာ ရွာျပီး ေနာက္ကျဖစ္ေစ ေရွ႕ဘက္ကျဖစ္ေစ"+
                "ZWSP လုပ္ေပးလိုက္တာပဲ ျဖစ္ပါတယ္။ ေရွ႕လား ေနာက္လား ဆိုတာကေတာ့ ျမန္မာစာ အေရးအသားေပၚ မူတည္ေနတယ္လို႕ ဆိုရမယ္။ "+
                "သေဝထိုး ဆိုရင္ ZWSP ကိုေရွ႕ဘက္ကေန ကပ္ေပးျပီး တျခား ဟာေတြ ဆိုရင္ေတာ့ ေနာက္ဘက္ကေန ကပ္လိုက္ပါတယ္။ "+
                "အခုေလာေလာဆယ္ ျမန္မာစာ Pangram ကိုစမ္းထားပါတယ္။\n\n " +
                "(ZWSP) : သီဟို႒္မွ ဉာဏ္ၾကီးရွင္သည္ အာယုဝဍဎန ေဆးၫႊန္းစာကို ဇလြန္ ေစ်းေဘး ဗာဒံပင္ထက္ အဓိ႒ာန္လ်က္ ဂဃနဏ ဖတ္ခဲ့သည္္ \n\n"+
                "ဒါကေတာ့ ZWSP မလုပ္ထားတဲ့ Pangram ပဲျဖစ္ပါတယ္။ Compare လုပ္ၾကည့္ႏိုင္ပါတယ္။";
        
        //String NonZWSP = 
        String NonZWSP = "\n\n(NON-ZWSP) : သီဟို႒္မွ ဉာဏ္ၾကီးရွင္သည္ အာယုဝဍဎန ေဆးၫႊန္းစာကို ဇလြန္ ေစ်းေဘး ဗာဒံပင္ထက္ အဓိ႒ာန္လ်က္ ဂဃနဏ ဖတ္ခဲ့သည္္";
        
        txtZawgyi.setText(insert_zwsp(myStr) + NonZWSP);
        
    }
    
    public String insert_zwsp(String str) {

      
    	String[] consonants = {"္"};

    	List<String> cons = Arrays.asList(consonants);

    	int arrLength = str.length();

    	String newStr = "";

    	for (int i = 0; i < arrLength; i++) {

    		String chr = str.substring(i, i + 1);
    		
    		String next = "";
    		
    		//To validate for last char in the string. Otherwise it will get error when reach last char
    		if((i+1) < arrLength){
    			next = str.substring(i+1,i+2);
    		}
    		
    		if (cons.contains(chr)) {

    			//To check for "Nga Thet"
    			if (chr.equals("္")) {
    				
    				//If "wit sa hnit lone pauk" not follow behid "Nga Thet", then it will do for ZWSP
    				 if(!next.equals("း")){
                         if(!next.equals("့")){
                             newStr += chr + "\u200B"; // ZWSP
                         }else if(next.equals("့")){
                             newStr += chr; // ZWSP
                         }
                     }
                     else{
                         newStr += chr;
                     }
    				//ာ
    				

    			}
    			
    			else if (chr.equals("ာ")) {
    				
    				//If "wit sa hnit lone pauk" not follow behid "Nga Thet", then it will do for ZWSP
    				if(!next.equals("း")){
    					newStr += chr + "\u200B"; // ZWSP
    				}else{
    					newStr += chr;
    				}
    				//ာ
    				

    			}
    			
    			
    		//If "Tha Way Htoe", ZWSP will be in front 
    		}else if(chr.equals("ေ")){
				newStr += "\u200B" + chr; // ZWSP
			}
    		else{
				
				newStr += chr;
				
			}
    	}
    	
    	return newStr;
    }
}